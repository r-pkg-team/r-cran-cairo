r-cran-cairo (1.6-2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 20 Dec 2023 11:49:15 +0100

r-cran-cairo (1.6-1-2) unstable; urgency=medium

  * Fix clean
    Closes: #1045256

 -- Andreas Tille <tille@debian.org>  Mon, 23 Oct 2023 10:01:07 +0200

r-cran-cairo (1.6-1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 25 Aug 2023 11:35:48 +0200

r-cran-cairo (1.6-0-4) unstable; urgency=medium

  * Rebuild with r-graphics-engine set by r-base

 -- Andreas Tille <tille@debian.org>  Thu, 06 Jul 2023 09:18:25 +0200

r-cran-cairo (1.6-0-2) unstable; urgency=medium

  * Disable reprotest
  * Rebuild against r-base 4.3.1 to deal with graphics API change
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 30 Jun 2023 15:29:21 +0200

r-cran-cairo (1.6-0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 11 Jul 2022 14:15:39 +0200

r-cran-cairo (1.5-15-3) unstable; urgency=medium

  * Team upload.
  * Re-build with R 4.2.0-1

 -- Nilesh Patra <nilesh@debian.org>  Sat, 30 Apr 2022 23:12:54 +0530

r-cran-cairo (1.5-15-2) unstable; urgency=medium

  * Team upload.
    + Rebuild with new r-base

 -- Nilesh Patra <nilesh@debian.org>  Mon, 28 Mar 2022 03:26:21 +0530

r-cran-cairo (1.5-15-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 1.5-15

 -- Nilesh Patra <nilesh@debian.org>  Sun, 20 Mar 2022 02:17:53 +0530

r-cran-cairo (1.5-14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <nilesh@debian.org>  Fri, 31 Dec 2021 10:24:01 +0000

r-cran-cairo (1.5-12.2-2) unstable; urgency=medium

  * Team upload.
    + Rebuild against R version 4.1.1-2 (Closes: #996431)
  * Bump Standards-Version to 4.6.0 (no changes needed)
  * Add patch ac-path-pkgconfig.patch: Use cross-build compatible macro for
    finding pkg-config.

 -- Nilesh Patra <nilesh@debian.org>  Tue, 19 Oct 2021 07:16:35 +0000

r-cran-cairo (1.5-12.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Wed, 29 Jul 2020 09:57:41 +0200

r-cran-cairo (1.5-12-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Tue, 14 Apr 2020 17:47:06 +0200

r-cran-cairo (1.5-11-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 20 Mar 2020 10:59:19 +0100

r-cran-cairo (1.5-10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0

 -- Dylan Aïssi <daissi@debian.org>  Fri, 19 Jul 2019 12:40:14 +0200

r-cran-cairo (1.5-9-3) unstable; urgency=medium

  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jun 2018 16:43:05 +0200

r-cran-cairo (1.5-9-2) unstable; urgency=medium

  * add missing copyright
  * Moved packaging from SVN to Git
  * Convert from cdbs to dh-r
  * Standards-Version: 4.1.1
  * debhelper 10
  * Canonical homepage for CRAN packages
  * Testsuite: autopkgtest-pkg-r
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Thu, 19 Oct 2017 09:37:07 +0200

r-cran-cairo (1.5-9-1) unstable; urgency=medium

  * Initial upload (Closes: #835087)

 -- Andreas Tille <tille@debian.org>  Mon, 22 Aug 2016 12:29:08 +0200
